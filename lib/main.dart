import 'package:flutter/material.dart';
import 'package:marche_admin_web/routing/route_name.dart';
import 'package:marche_admin_web/routing/router.dart';
import 'package:provider/provider.dart';

import 'locator.dart';
import 'providers/app_provider.dart';

void main() {
  setupLocator();
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider.value(value: AppProvider.init()),
  ], child: MyApp()));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Marche Admin',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      onGenerateRoute: generateRoute,
      initialRoute: LoginRoute,
    );
  }
}
