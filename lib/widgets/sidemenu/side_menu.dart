import 'package:flutter/material.dart';
import 'package:marche_admin_web/widgets/sidemenu/side_menu_tablet_desktop.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'menu_side_mobile.dart';

class SideMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      breakpoints: ScreenBreakpoints(tablet: 600, desktop: 1460, watch: 300),
      mobile: SideMenuMobile(),
      desktop: SideMenuTabletDesktop(),
    );
  }
}
