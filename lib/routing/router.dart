import 'package:flutter/material.dart';
import 'package:marche_admin_web/pages/home/home.dart';
import 'package:marche_admin_web/pages/login/login.dart';
import 'package:marche_admin_web/pages/orders/orders.dart';
import 'package:marche_admin_web/pages/products/products.dart';
import 'package:marche_admin_web/pages/registration/registration.dart';
import 'package:marche_admin_web/pages/user/users.dart';
import 'package:marche_admin_web/routing/route_name.dart';
import 'package:marche_admin_web/widgets/layout/layout.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  print('generateRoute: ${settings.name}');
  switch (settings.name) {
    case HomeRoute:
      return _getPageRoute(HomePage());
    case UsersRoute:
      return _getPageRoute(UsersPage());
    case ProductsRoute:
      return _getPageRoute(ProductsPage());
    case OrdersRoute:
      return _getPageRoute(OrdersPage());
    case LoginRoute:
      return _getPageRoute(LoginPage());
    case RegistrationRoute:
      return _getPageRoute(RegistrationPage());
    case LayoutRoute:
      return _getPageRoute(LayoutTemplate());
    default:
      return _getPageRoute(LoginPage());
  }
}

PageRoute _getPageRoute(Widget child) {
  return MaterialPageRoute(
    builder: (context) => child,
  );
}
